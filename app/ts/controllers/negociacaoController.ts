
import { MensagemView, NegociacoesView } from "../views/index";
import { Negociacao, Negociacoes, NegociacaoParcial } from "../models/index";
import { DomInject, Throttle } from "../helpers/decorators/index";
import { NegociacaoService, HandlerFunction } from "../services/index";
import { imprime } from "../helpers/utils";

export class NegociacaoController {

    @DomInject('#data')
    private _inputData: JQuery;
    @DomInject('#quantidade')
    private _inputQuantidade: JQuery;
    @DomInject('#valor')
    private _inputValor: JQuery;

    private _negociacoes = new Negociacoes();
    private _negociacoesView = new NegociacoesView('#negociacoesView');
    private _mensagemView = new MensagemView('#mensagemView');
    private _service = new NegociacaoService();

    constructor() {
        /*this._inputData =  $('#data');
        this._inputValor =  $('#quantidade');
        this._inputQuantidade = $('#valor');   
        */

        this._negociacoesView.update(this._negociacoes);
    }

    @Throttle(500)
    adiciona(): void {

        let data = new Date(this._inputData.val().replace(/-/g, ','));

        if (!this.ehDiaUtil(data)) {
            this._mensagemView.update('Permitido lançar negociação somente em dias uteis');
            return;
        }

        const negociacao = new Negociacao(
            data,
            parseInt(this._inputQuantidade.val()),
            parseFloat(this._inputValor.val()));

        this._negociacoes.adiciona(negociacao);

        this._negociacoesView.update(this._negociacoes);
        this._mensagemView.update('Negociação adicionada com sucesso');

        imprime(negociacao, this._negociacoes);
    }

    @Throttle(500)
    async importarDados() {


        const isOK: HandlerFunction = (res: Response) => {

            if (res.ok) {
                return res;
            } else {
                throw new Error(res.statusText);
            }

        }

        try {
            const negociacoesParaImportar = await this._service.obterNegociacoes(isOK);
            const negociacoesJaImportadas = this._negociacoes.paraArray();

            negociacoesParaImportar.filter(n => !negociacoesJaImportadas.some(jaIimportada => n.ehIgual(jaIimportada))).forEach(negociacao => {
                this._negociacoes.adiciona(negociacao);
                this._negociacoesView.update(this._negociacoes);
            });
        } catch (err) {
            this._mensagemView.update(err.message);
        }



    }

    private ehDiaUtil(data: Date) {
        return data.getDay() != DiaDaSemana.SABADO && data.getDay() != DiaDaSemana.DOMINGO;
    }
}

enum DiaDaSemana {
    DOMINGO, SEGUNDA, TERCA, QUARTA, QUINTA, SEXTA, SABADO
}