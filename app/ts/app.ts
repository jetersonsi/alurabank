import { Negociacao } from "./models/negociacao";
import { NegociacaoController } from "./controllers/negociacaoController";

const negociacao = new Negociacao(new Date(), 1, 100);
//negociacao._quantidade = 3
console.log(negociacao);

const controller = new NegociacaoController();
$('.form').submit(controller.adiciona.bind(controller));
$('#botao-importar').click(controller.importarDados.bind(controller));