export function TempoExecucao(segundos:boolean = false) {
    return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {

        const methodOrig = descriptor.value;
        descriptor.value = function (...args: any[]) {
            let unidade = 'ms';
            let divisor = 1;
            
            if(segundos){
                unidade = 's';
                divisor = 1000;
            }
            console.log('-------------');
            console.log(`Parametros passados para o metodo ${propertyKey}: ${JSON.stringify(args)}`);
            const t1 = performance.now();
            const retorno = methodOrig.apply(this, args);
            const t2 = performance.now();
            console.log(`O retorno do mpetodo ${propertyKey} é: ${JSON.stringify(retorno)}`);
            console.log(`O médodo ${propertyKey} demorou ${(t2-t1)/divisor}${unidade}`);
            
            return retorno;
        }
        return descriptor;

    }
}