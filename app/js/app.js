System.register(["./models/negociacao", "./controllers/negociacaoController"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var negociacao_1, negociacaoController_1, negociacao, controller;
    return {
        setters: [
            function (negociacao_1_1) {
                negociacao_1 = negociacao_1_1;
            },
            function (negociacaoController_1_1) {
                negociacaoController_1 = negociacaoController_1_1;
            }
        ],
        execute: function () {
            negociacao = new negociacao_1.Negociacao(new Date(), 1, 100);
            console.log(negociacao);
            controller = new negociacaoController_1.NegociacaoController();
            $('.form').submit(controller.adiciona.bind(controller));
            $('#botao-importar').click(controller.importarDados.bind(controller));
        }
    };
});
