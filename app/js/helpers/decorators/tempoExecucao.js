System.register([], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    function TempoExecucao(segundos = false) {
        return function (target, propertyKey, descriptor) {
            const methodOrig = descriptor.value;
            descriptor.value = function (...args) {
                let unidade = 'ms';
                let divisor = 1;
                if (segundos) {
                    unidade = 's';
                    divisor = 1000;
                }
                console.log('-------------');
                console.log(`Parametros passados para o metodo ${propertyKey}: ${JSON.stringify(args)}`);
                const t1 = performance.now();
                const retorno = methodOrig.apply(this, args);
                const t2 = performance.now();
                console.log(`O retorno do mpetodo ${propertyKey} é: ${JSON.stringify(retorno)}`);
                console.log(`O médodo ${propertyKey} demorou ${(t2 - t1) / divisor}${unidade}`);
                return retorno;
            };
            return descriptor;
        };
    }
    exports_1("TempoExecucao", TempoExecucao);
    return {
        setters: [],
        execute: function () {
        }
    };
});
